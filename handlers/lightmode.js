const lastInvoke = {};

const triggers = [
    'light mode?',
    'light theme',
    'light discord',
    'bright theme',
    'bright mode',
    'bright discord'
];

const COOLDOWN = 1000 * 3600;

module.exports = {
    message: async obj => {
        const { msg } = obj;

        if (msg.author.bot)
            return;

        const match = msg.content.toLowerCase();

        if (
            (match.includes('ew') || match.includes('why') || match.includes('?') || match.includes('using'))
            && (match.includes('light') || match.includes('bright'))
            && (match.includes('mode') || match.includes('theme') || match.includes('discord'))
            && (Date.now() - (lastInvoke[msg.channel.id] ?? 0)) > COOLDOWN
        ) {
            lastInvoke[msg.channel.id] = Date.now();
            await msg.channel.send(`haha you use light mode? 🤣 lmao why do you hate yourself, it legit makes my eyes hurt why would anyone use this I'm so quirky for using dark theme like 99% of the other discord users`);
        }
    }
};
