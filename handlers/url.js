const execFile = require('util').promisify(require('child_process').execFile);

const replaceUrls = [
    {
        match: 'https://spotify.link',
        type: 'COMMAND',
        command: 'curl',
        args: ['-si', '${url}', '|', 'grep', '-oP', '"https://open\.spotify\.com/track/[a-zA-Z0-9]+"']
    }, 
];

const replaceUrl = async (url, rule) => {
    if (rule.type == 'STRING') {
        return rule.replace(url);
    }

    if (rule.type == 'COMMAND') {
        const { stdout } = await execFile(rule.command, rule.args.map(x => x.replace('${url}', url)), { shell: true });

        return stdout;
    }
}

const allowedMentions = { repliedUser: false };

module.exports = {
    message: async obj => {
        const { msg } = obj;

        const urls = [];

        for (const part of msg.content.split(' ')) {
            for (const r of replaceUrls) {
                if (!part.includes(r.match)) {
                    continue;
                }

                urls.push(await replaceUrl(part, r));
                break;
            }
        }

        if (urls.length == 0) {
            return;
        }

        await msg.reply({ content: urls.join(' '), allowedMentions });
        await msg.suppressEmbeds(true);
    }
};
