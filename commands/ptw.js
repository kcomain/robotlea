

const { ActionRowBuilder, ButtonBuilder, ButtonStyle, InteractionCollector, inlineCode } = require('discord.js');
const helper = require('../helper.js');
const anilist = require('anilist-node');
const { capitalize } = require('lodash');
const Anilist = new anilist();
const axios = require('axios');

let SEASONS = {};

for (let i = 1; i <= 12; i++) {
    switch(i) {
        case 1:
        case 2:
        case 3:
            SEASONS[i] = 'Winter';
            break;
        case 4:
        case 5:
        case 6:
            SEASONS[i] = 'Spring';
            break;
        case 7:
        case 8:
        case 9:
            SEASONS[i] = 'Summer';
            break;
        default:
            SEASONS[i] = 'Fall';
    }
}

const FORMAT = {
    TV_SHORT: 'TV Short',
    MOVIE: 'Movie',
    SPECIAL: 'Special',
    MUSIC: 'Music',
    MANGA: 'Manga',
    NOVEL: 'Novel',
    ONE_SHOT: 'One Shot',
};

const STATUS_EMOJI = {
    COMPLETED: '🟢',
    PAUSED: '🟠',
    PLANNING: '🟡',
    REPEATING: '🔁',
    CURRENT: '🔵',
    DROPPED: '🔴'
};

const STATUS_ORDER = ['COMPLETED', 'DROPPED', 'PAUSED', 'REPEATING', 'CURRENT', 'PLANNING'];

const FILTERS = {
    year: ["y"],
    genres: ["genre"],
    tags: ["tag"],
    studios: ["studio"],
    averageScore: ["score"],
    episodes: ["eps"],
    status: ["s"],
    format: ["f"],
    id: []
};

const SHORTCUTS = {
    tags: {
        bl: "boys' love",
        cgdct: "cute girls doing cute things",
        cbdct: "cute boys doing cute things",
    },
    genres: {
        sol: "slice of life"
    }
}

const FILTER_REGEX = new RegExp('([a-z]+)([><=]{1,2})([a-z0-9,\_]+)');

let filterList = () => {
    let list = [];

    for (const key in FILTERS) {
        list.push(`\`${key} (${FILTERS[key].join(',')})\``)
    }

    return list;
};


const formatList = list => {
    if (!list || list.length == 0) return 'Empty';
    return `\`${list.join('` - `')}\``;
}

const normalizeScore = (score, format) => {
    if (format == 'POINT_3') {
        switch(score) {
            case 3:
                return ':)';
            case 2:
                return ':|';
            case 1:
                return ':(';
        }
    }

    if (format == 'POINT_100')
        score /= 10;

    if (format == 'POINT_5')
        score *= 2;

    return score.toFixed(1);
}

const listQuery =
`query ($userId: Int, $type: MediaType) {
    MediaListCollection(userId: $userId, type: $type, status_in: [PLANNING]) {
        lists { 
            name
            status
            entries {
                id
                media {
                    id
                    title { romaji english native userPreferred }
                    studios {
                        nodes { name }
                    }
                    averageScore
                    coverImage { large color }
                    episodes
                    format
                    status
                    startDate { year month day }
                    duration
                    genres
                    tags { name isMediaSpoiler }
                    isAdult
                    siteUrl
                }
                status
                score
                progress
                repeat
                priority
                private
                notes
                hiddenFromStatusLists
                advancedScores
                startedAt { year month day }
                completedAt { year month day }
                updatedAt
                createdAt
            }
        }
    }
}`;

const scoreQuery = 
`query ($id: Int, $userIds: [Int], $page: Int = 1) {
    Page (page: $page, perPage: 50) {
        mediaList (mediaId: $id, userId_in: $userIds, sort:UPDATED_TIME_DESC) {
            id
            status
            score
            user {
                name
                mediaListOptions { scoreFormat }
            }
        }
    }
}`;

module.exports = {
    command: ['ptw', 'planned'],
    usage: '[random/oldest]',
    argsRequired: 0,
    call: async obj => {
        const { argv, msg, db, client } = obj;

        let type = 'random';

        if (argv.length > 1 && argv[1] == 'oldest') {
            type = 'oldest';
        }

        let anilistUserId;
        let message;

        const userIdDb = await db.get(`anilist_user_${msg.author.id}`);

        const guildMemberIds = [];

        for await (const [key, value] of db.iterator()) {
            if (!key.startsWith('anilist_user_')) continue;

            const memberId = key.split('_').pop();

            if (msg.author.id == memberId) continue;
            if (!msg.guild.members.cache.has(memberId)) continue;

            guildMemberIds.push(value);
        };

        if (userIdDb == null) {
            const linkButton = new ButtonBuilder()
            .setCustomId('link-anilist-account')
            .setLabel('Link AniList Account')
            .setStyle(ButtonStyle.Primary);

            const row = new ActionRowBuilder()
            .addComponents(linkButton);

            const embeds = [
                {
                    color: helper.mainColor,
                    description: 'Nyo AniList account winked, cwick the button to do so.',
                }
            ];

            message = await msg.channel.send({ embeds, components: [row] });

            const filter = 
                i => i.customId == 'link-anilist-modal' &&
                i.user.id == msg.author.id &&
                i.message.id == message.id;

            const collector = new InteractionCollector(client, {
                time: 5 * 60 * 1000,
                filter
            });

            while (anilistUserId == null) {
                const interaction = await new Promise(resolve => collector.on('collect', i => resolve(i)));

                try {
                    const username = interaction.fields.getTextInputValue('anilist-user-input');
                    const user = await Anilist.user.profile(username);

                    anilistUserId = user.id;
                } catch(e) {
                    // do nothing
                }
            }
        } else {
            anilistUserId = userIdDb;
        }

        const response = await axios.post('https://graphql.anilist.co',
            {
                query: listQuery,
                variables: {
                    userId: anilistUserId,
                    type: 'ANIME'
                }
            }
        );

        const lists = response.data.data.MediaListCollection.lists;

        if (lists.length == 0) {
            throw "Nyo pwanning anime found!";
        }

        const planningList = lists[0];

        let plannedEntries = (planningList.entries ?? []).filter(x => !x.media.isAdult && x.media.status != 'NOT_YET_RELEASED');

        const totalEntries = plannedEntries.length;

        plannedEntries.forEach(x => x.media.year = x.media.startDate.year);

        if (argv.length > 1) {
            for (const item of argv.slice(1)) {
                const arg = item.toLowerCase();
                const filter = FILTER_REGEX.exec(arg);

                if (!filter || filter.length < 4) 
                    throw "Invalid filter, please use format `key=value` (e.g. `status=finished`, `year<=2013` or `genre=action,comedy`)";

                let [,searchKey,operator,value] = filter;

                let filterKey;

                for (const key in FILTERS) {
                    if (key == searchKey || FILTERS[key].includes(searchKey)) {
                        filterKey = key;
                        break;
                    }
                }

                if (filterKey === undefined) 
                    throw "Filter not found. Available filters: " + filterList().join(',');

                if (Array.isArray(plannedEntries[0].media[filterKey])) {
                    let values = value.split(',');

                    if (filterKey in SHORTCUTS) {
                        values = values.map(x => SHORTCUTS[filterKey][x] ?? x);
                    }

                    values = values.map(x => x.replaceAll('_', ' '));

                    for (const v of values) {
                        if (filterKey == 'tags') {
                            plannedEntries = plannedEntries.filter(
                                x => x.media[filterKey].find(y => y.name.toLowerCase() == v) !== undefined
                            );
                        } else {
                            plannedEntries = plannedEntries.filter(
                                x => x.media[filterKey].find(y => y.toLowerCase() == v) !== undefined
                            );
                        }
                    }
                } else {
                    if (!isNaN(parseInt(value))) {
                        if (typeof plannedEntries[0].media[filterKey] !== 'number')
                            throw `Key \`${filterKey}\` is not a number.`;

                        value = parseInt(value);

                        switch(operator) {
                            case '=':
                                plannedEntries = plannedEntries.filter(x => x.media[filterKey] == value);
                                break;
                            case '>=':
                                plannedEntries = plannedEntries.filter(x => x.media[filterKey] >= value);
                                break;
                            case '<=':
                                plannedEntries = plannedEntries.filter(x => x.media[filterKey] <= value);
                                break;
                            case '<':
                                plannedEntries = plannedEntries.filter(x => x.media[filterKey] < value);
                                break;
                            case '>':
                                plannedEntries = plannedEntries.filter(x => x.media[filterKey] > value);
                                break;
                            default:
                                throw "Unknown operator. Valid operators: `=`, `>=`, `<=`, `>`, `<`";
                        }
                    } else {
                        if (typeof plannedEntries[0].media[filterKey] !== 'string')
                            throw `Key \`${filterKey}\` is not a string.`;

                        plannedEntries = plannedEntries.filter(x => x.media[filterKey].toLowerCase() == value.toLowerCase());
                    }                   
                }
            }
        }

        if (plannedEntries.length == 0) {
            throw "Nyo pwanning anime found! (or all entries filtered out)";
        }

        const randomPlanning = plannedEntries[Math.floor(Math.random() * plannedEntries.length)];

        const { media } = randomPlanning;

        const scoresRequest = await axios.post('https://graphql.anilist.co',
            {
                query: scoreQuery,
                variables: {
                    id: media.id,
                    userIds: guildMemberIds
                }
            }
        );

        let scores = scoresRequest.data?.data?.Page?.mediaList ?? [];

        const title = media.title.english ?? media.title.romaji ?? media.title.native;

        const season = media.startDate?.month ? `${SEASONS[media.startDate.month]} ${media.startDate.year}` : 'Unknown';
        const episodeCount = media.episodes ? media.episodes.toString() : '?';

        const embeds = [
            {
                color: media.coverImage?.color ? Number(`0x${media.coverImage.color.substring(1)}`) : helper.mainColor,
                url: media.siteUrl,
                thumbnail: { url: media.coverImage.large },
                title: title,
                fields: [
                    {
                        name: 'Avg Score',
                        value: `${media.averageScore}/100`,
                        inline: true
                    }, {
                        name: 'Format',
                        value: FORMAT[media.format] ?? media.format,
                        inline: true
                    }, {
                        name: 'Season',
                        value: season,
                        inline: true
                    }, {
                        name: 'Episodes',
                        value: episodeCount,
                        inline: true
                    }, {
                        name: 'Duration',
                        value: `${media.duration} min`,
                        inline: true
                    }, {
                        name: 'Studio',
                        value: media.studios?.nodes[0]?.name ?? 'Unknown',
                        inline: true
                    }, {
                        name: 'Genres',
                        value: formatList(media.genres)
                    }, {
                        name: 'Tags',
                        value: formatList(media.tags.filter(x => !x.isMediaSpoiler).slice(0, 5).map(x => x.name))
                    }
                ]
            }
        ];

        if (scores.length > 0) {
            scores = scores.sort((a, b) => { return STATUS_ORDER.indexOf(a.status) - STATUS_ORDER.indexOf(b.status) });

            let scoresText = "";

            for (const [index, score] of scores.entries()) {
                if (index > 0) {
                    scoresText += index % 3 == 0 ? '\n' : ' - ';
                }

                scoresText += `${STATUS_EMOJI[score.status]} ${score.user.name}`;
                
                if (score.score > 0) {
                    const normalizedScore = normalizeScore(score.score, score.user.mediaListOptions?.scoreFormat);

                    scoresText += ` (${inlineCode(normalizedScore)})`;
                }
            }

            embeds[0].fields.push({
                name: 'Scores',
                value: scoresText
            });
        }


        let footerText = "";

        if (media.title.romaji && media.title.romaji != title) { 
            footerText = media.title.romaji;
        }

        if (footerText) footerText += " • "

        if (plannedEntries.length < totalEntries) {
            footerText += `${plannedEntries.length} filtered entries (${totalEntries} total)`;
        } else {
            footerText += `${totalEntries} list entries`;
        }

        embeds[0].footer = { text: footerText };

        if (message == null) {
            await msg.channel.send({ embeds });
        } else {
            await message.edit({ embeds });
        }

        return;
    }
}
