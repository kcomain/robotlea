const YEAR_0 = 1560275700_000;
const FIRST_RAIN = 1560275700_000 + 4050_000;
const RAIN_OFFSET = 4850_000;
const THUNDER_OFFSET = RAIN_OFFSET * 4;
const RAIN_DURATION = 1000_000;

const FORMATS = ['t', 'T', 'd', 'D', 'f', 'F', 'R']

module.exports = {
    command: ['rain', 'thunder'],
    description: [
        "Show rain/thunder times.",
    ],
    call: async obj => {
        const { msg } = obj;
        const argv = msg.content.split(' ');

        let format = 'R';

        if (argv.length > 1) {
            const _format = argv[1].trim();

            if (FORMATS.includes(_format)) {
                format = _format;
            }
        }

        const rains = {
            name: 'Rains',
            value: '',
            inline: true
        };

        let time = Date.now() - RAIN_OFFSET;
        let currentRainIndex = Math.ceil((time - FIRST_RAIN) / RAIN_OFFSET);

        for (let i = 0; i < 7; i++) {
            if (i > 0)
                rains.value += '\n';

            if (i == 1)
                rains.value += '\n';

            rains.value += `<t:${Math.floor((FIRST_RAIN + RAIN_OFFSET * (currentRainIndex + i)) / 1000)}:${format}>`;
        }

        const thunders = {
            name: 'Thunders',
            value: '',
            inline: true
        };

        time = Date.now() - THUNDER_OFFSET;
        let currentThunderIndex = Math.ceil((time - FIRST_RAIN) / THUNDER_OFFSET);

        for (let i = 0; i < 7; i++) {
            if (i > 0)
                thunders.value += '\n';

            if (i == 1)
                thunders.value += '\n';

            thunders.value += `<t:${Math.floor((FIRST_RAIN + THUNDER_OFFSET * (currentThunderIndex + i)) / 1000)}:${format}>`;
        }

        const embed = {
            fields: [rains, thunders],
            footer: { text: 'start times, rain lasts 16m40s' }
        };

        return { embeds: [embed] };
    }
};
